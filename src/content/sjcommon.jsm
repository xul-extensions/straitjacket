var EXPORTED_SYMBOLS = ["Common","Cc","Ci","Cu","SJPrefListener"]
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");
Common = {
	contentPrefs : Cc["@mozilla.org/content-pref/service;1"].getService(Ci.nsIContentPrefService2),
	logoutput : false,
	sandboxen : {},
	strBundle : null,
	optionsDlg : null,
	prefBranch : Services.prefs.getBranch("extensions.straitjacket."),
	ID : "{dad187b1-3047-479c-858c-8d785479189c}",
	uninstall : false,
	/**
	 * Log output to toolkit error console if enabled.
	 */ 
	printlog:function(item) {
		if(Common.logoutput==true) {
			Services.console.logStringMessage("[Straitjacket:]"+item);
		}
	},
	reloadSandboxen : function(){
		let data = Common.prefBranch.getCharPref("sandboxen");
		try {
			Common.sandboxen = JSON.parse(data);
		} catch (e){
			Cu.reportError("[Straitjacket:] Unable to parse sandbox list.");
			Common.sandboxen = {};
		}
	},
	/**
	 * Set the selected item in the combo to the assigned sandbox if present.
	 */
	configureMenulist : function(menulist,sandboxID,sPrefix){
		for(let i = 0;i < menulist.itemCount;i++){
			if(menulist.getItemAtIndex(i).id.replaceAll(sPrefix,'') == sandboxID){
				menulist.selectedIndex = i;
				break;
			}
		}
	},
  /**
     * Check whether extension is being uninstalled and undo if not; 
     * clean up settings when browser is restarting after a confirmed uninstall. 
     */
    uninstallHandler :{
        onUninstalling:function(addon){
            if(addon.id==Common.ID){
                Common.uninstall=true;
            }
        },
        onOperationCancelled: function(addon) {
            if (addon.id == Common.ID) {
                Common.uninstall = (addon.pendingOperations & AddonManager.PENDING_UNINSTALL) != 0;
            }
        },   
        observe : function(aSubject,aTopic,aData){
            if(aTopic == "quit-application" && Common.uninstall == true) {
                Services.prefs.deleteBranch("extensions.straitjacket.");
                Common.contentPrefs.removeByName("straitjacket-sandbox",null);
            }
        }
    }
}
function SJPrefListener(branch_name, callback) {
	// Keeping a reference to the observed preference branch or it will get
	// garbage collected.
	this._branch = Services.prefs.getBranch(branch_name);
	this._branch.QueryInterface(Ci.nsIPrefBranch2);
	this._callback = callback;
}

SJPrefListener.prototype.observe = function(subject, topic, data) {
	if (topic == 'nsPref:changed')
		this._callback(this._branch, data);
};

SJPrefListener.prototype.register = function() {
	this._branch.addObserver('', this, false);
	let that = this;
	this._branch.getChildList('', {}).
	forEach(function (pref_leaf_name){ 
		that._callback(that._branch, pref_leaf_name); 
	});
};

SJPrefListener.prototype.unregister = function() {
	if (this._branch)
		this._branch.removeObserver('', this);
};