if("undefined" == typeof (Straitjacket)) {
	var Straitjacket = {};
}
Components.utils.import("chrome://straitjacket/content/sjcommon.jsm",Straitjacket);
Cu.import("chrome://straitjacket/content/sjcore.jsm",Straitjacket);
Cu.import("resource://gre/modules/Services.jsm");
Straitjacket.PageInfo = {
	docURI : null,
	sandboxEntry : null,
	chkDefault : null,
	cmbSandboxen : null,
	init : function(){
		Straitjacket.PageInfo.sandboxEntry = document.getElementById("permsjSandboxRow");
		Straitjacket.PageInfo.chkDefault = document.getElementById("chk-sjpageinfo-default");
		Straitjacket.PageInfo.cmbSandboxen = document.getElementById("mnulst-sjpageinfo");
		Straitjacket.Common.reloadSandboxen();
		Straitjacket.PageInfo.docURI = window.opener.gBrowser.selectedBrowser.currentURI;
		if(Straitjacket.Common.sandboxen.length == 0 || !Straitjacket.PageInfo.docURI.scheme.startsWith('http')){
			Straitjacket.PageInfo.sandboxEntry.setAttribute("hidden","true");
			Straitjacket.Common.printlog("Not on a webpage or no sandboxen defined, returning. "+Straitjacket.PageInfo.docURI.spec);
			return;
		}
		Straitjacket.PageInfo.chkDefault.addEventListener("command",Straitjacket.PageInfo.onChkDefault,false);
		Straitjacket.PageInfo.cmbSandboxen.addEventListener("select",Straitjacket.PageInfo.onMenulist,false);
		Straitjacket.PageInfo.loadSandboxen();
		Straitjacket.Common.contentPrefs.getByDomainAndName(Straitjacket.PageInfo.docURI.host,
			"straitjacket-sandbox",null,Straitjacket.PageInfo.pgInfoContentPrefHandler);
	},
	/**
	 * Loads sandboxes for adding in dropdown.
	 */
	loadSandboxen : function(){
		for (let i = 0; i < Object.values(Straitjacket.Common.sandboxen).length;i++){ 
			let item = Straitjacket.PageInfo.cmbSandboxen.appendItem(Object.values(Straitjacket.Common.sandboxen)[i].name,Object.values(Straitjacket.Common.sandboxen)[i].id);
			item.setAttribute('id', 'straitjacket-pginfo--'+Object.values(Straitjacket.Common.sandboxen)[i].id);
		}
	},
	/**
	 * 'Use default' checkbox handler.
	 */
	onChkDefault : function(event){
		Straitjacket.PageInfo.cmbSandboxen.disabled = event.target.checked;
		if(event.target.checked){
			Straitjacket.Common.contentPrefs.removeBySubdomainAndName(Straitjacket.PageInfo.docURI.host,"straitjacket-sandbox",null);
			Straitjacket.PageInfo.cmbSandboxen.selectedIndex=-1;
			Straitjacket.Common.printlog("Deleted sandbox assignment for "+Straitjacket.PageInfo.docURI.host);
		} else {
			Straitjacket.Common.contentPrefs.set(Straitjacket.PageInfo.docURI.host,"straitjacket-sandbox",
			Straitjacket.PageInfo.cmbSandboxen.selectedItem.value,null);
			Straitjacket.Common.printlog("Assigned "+Straitjacket.SJCore.getSandboxbyNameOrID(Straitjacket.PageInfo.cmbSandboxen.selectedItem.value,true)+" to "+Straitjacket.PageInfo.docURI.host);
		}
	},
 	/**
	 * Sandboxes menulist event handler.
	 */
	onMenulist : function(event){
		Straitjacket.Common.printlog("Assigning "+Straitjacket.PageInfo.docURI.host+" to sandbox "+Straitjacket.SJCore.getSandboxbyNameOrID(event.target.selectedItem.value,true));
		Straitjacket.Common.contentPrefs.set(Straitjacket.PageInfo.docURI.host,"straitjacket-sandbox",
		event.target.selectedItem.value,null);
	},
	/**
	 * nsIContentPrefCallback2 implementation used for removing or changing sandbox 
	 * assigned to current page.
	 */
	pgInfoContentPrefHandler : {
		success : false,
		handleCompletion : function(reason){
			if(reason == Straitjacket.Ci.nsIContentPrefCallback2.COMPLETE_ERROR || !Straitjacket.PageInfo.pgInfoContentPrefHandler.success){
				Straitjacket.Common.printlog("No sandbox assigned for "+Straitjacket.PageInfo.docURI.host);
				Straitjacket.PageInfo.chkDefault.setAttribute("checked","true");			
				Straitjacket.PageInfo.cmbSandboxen.setAttribute("disabled","true")
			}
		},
		handleError : function(error){
			Cu.reportError("[Straitjacket:] Error handling content preferences for page info. "+error);
		},
		handleResult : function(pref){
			Straitjacket.PageInfo.pgInfoContentPrefHandler.success = true;
			if(pref.domain == Straitjacket.PageInfo.docURI.host){
				Straitjacket.PageInfo.chkDefault.removeAttribute("checked");
				Straitjacket.PageInfo.cmbSandboxen.removeAttribute("disabled");
				Straitjacket.Common.configureMenulist(Straitjacket.PageInfo.cmbSandboxen,pref.value,'straitjacket-pginfo--');			
			} else {
				Straitjacket.PageInfo.chkDefault.setAttribute("checked","true");			
				Straitjacket.PageInfo.cmbSandboxen.setAttribute("disabled","true")
			}
		}
	},
	cleanup : function(){
		Straitjacket.PageInfo.cmbSandboxen.removeEventListener("command",Straitjacket.PageInfo.onMenulist);
		Straitjacket.PageInfo.chkDefault.removeEventListener("command",Straitjacket.PageInfo.onChkDefault);
	}
}
window.addEventListener("load", Straitjacket.PageInfo.init, false);
window.addEventListener("unload",Straitjacket.PageInfo.cleanup,false);