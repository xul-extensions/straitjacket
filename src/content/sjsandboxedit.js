if("undefined" == typeof (Straitjacket)) {
	var Straitjacket = {};
}
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("chrome://straitjacket/content/sjcommon.jsm",Straitjacket);
Cu.import("chrome://straitjacket/content/sjcore.jsm",Straitjacket);
Straitjacket.Sandboxedit = {
	bdSave : null,
	txtName : null,
	txtURL : null,
	colorPicker : null,
	sandbox : {},
	init : function() {
		Straitjacket.Sandboxedit.bdSave = document.getElementById("bd-straitjacket-sbsave");
		Straitjacket.Sandboxedit.txtName = document.getElementById("txtstraitjacket-sbname");
		Straitjacket.Sandboxedit.txtURL = document.getElementById("txtstraitjacket-sburl");
		Straitjacket.Sandboxedit.colorPicker = document.getElementById("sj-colorpicker");
		Straitjacket.Sandboxedit.colorPicker.color = Straitjacket.SJCore.randomColor();
		Straitjacket.Sandboxedit.txtName.addEventListener("input",Straitjacket.Sandboxedit.onTextChange,false);
		if(window.arguments[0] != null){
			Straitjacket.Sandboxedit.txtName.value = window.arguments[0].name;
			Straitjacket.Sandboxedit.txtURL.value = window.arguments[0].url;
			Straitjacket.Sandboxedit.colorPicker.color = window.arguments[0].color;
			Straitjacket.Sandboxedit.sandbox.id=window.arguments[0].id;
		} 
		if(Straitjacket.Sandboxedit.txtName.value.length == 0){
			Straitjacket.Sandboxedit.bdSave.setAttribute("disabled","true");
		}
	},
	onOk : function(){
		Straitjacket.Sandboxedit.sandbox.name = Straitjacket.Sandboxedit.txtName.value;
		Straitjacket.Sandboxedit.sandbox.url = Straitjacket.Sandboxedit.txtURL.value;
		Straitjacket.Sandboxedit.sandbox.color = Straitjacket.Sandboxedit.colorPicker.color;
		window.arguments[1](Straitjacket.Sandboxedit.sandbox); //callback parent and pass back sandbox.
	},
	onTextChange : function(){
		Straitjacket.Sandboxedit.txtName.value.length > 0 ? Straitjacket.Sandboxedit.bdSave.removeAttribute("disabled") :Straitjacket.Sandboxedit.bdSave.setAttribute("disabled","true"); 
	},
	onCancel : function(){
		window.arguments[2]();
	},
	cleanup : function(){
		Straitjacket.Sandboxedit.txtName.removeEventListener("input",Straitjacket.Sandboxedit.onTextChange);
	}
}
window.addEventListener("load", Straitjacket.Sandboxedit.init, false);
window.addEventListener("unload",Straitjacket.Sandboxedit.cleanup,false);