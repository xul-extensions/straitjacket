var EXPORTED_SYMBOLS = ["SJCore"];
if("undefined" == typeof (Straitjacket)) {
	var Straitjacket = {};
}
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("chrome://straitjacket/content/sjcommon.jsm",Straitjacket);
Cu.import("resource://gre/modules/Services.jsm");
SJCore = {
	//SJ_TAB_DATA_IDENTIFIER : "SJ_TAB_DATA_IDENTIFIER",
	README_URL : "resource://straitjacket/readme.html",
	WAIT_URL : "resource://straitjacket/wait.html",
	defaultBrowserStyle : null,
	defaultTabStyle : null,
	sessionStore : Cc["@mozilla.org/browser/sessionstore;1"].
	                       getService(Ci.nsISessionStore),
	/**
	 * Generate random ID for sandbox.
	 * @return {}
	 */
	generateID : function(){
		return (Math.floor(Math.random() * 1073741824)).toString(16);
	},
	randomColor : function(){
		return "#"+(Math.floor(Math.random() * 16777216)).toString(16);
	},
	getSandboxbyNameOrID : function(aSandbox,useID){
		for(let i = 0; i < Object.values(Straitjacket.Common.sandboxen).length;i++){
			if(useID){
				if(Object.values(Straitjacket.Common.sandboxen)[i].id == aSandbox){
					return Object.values(Straitjacket.Common.sandboxen)[i].name;
				}
			} else {
				if(Object.values(Straitjacket.Common.sandboxen)[i].name == aSandbox){
					return Object.values(Straitjacket.Common.sandboxen)[i].id;
				}
			}
		}
		return Services.scriptSecurityManager.NO_APP_ID;
	},
	openSandbox : function(aWindow,aId,aURL){
		if(!aId in Straitjacket.Common.sandboxen){
			return;
		}
		let mainWindow = aWindow.QueryInterface(Ci.nsIInterfaceRequestor)
	    	                    .getInterface(Ci.nsIWebNavigation)
	        	                .QueryInterface(Ci.nsIDocShellTreeItem)
	            	            .rootTreeItem
	                	        .QueryInterface(Ci.nsIInterfaceRequestor)
	                    	    .getInterface(Ci.nsIDOMWindow);
		if (!mainWindow) {
	  		Cu.reportError("[Straitjacket:] Error getting main window.");
	  		return;
		}
		let browser = mainWindow.gBrowser;
		if(!browser){
			Cu.reportError("[Straitjacket:] Error initializing the browser.");
			return;
		}
		let tab = browser.addTab(this.WAIT_URL);
		browser.selectedTab = tab;
		let tabBrowser = browser.getBrowserForTab(tab);
	 	let self = this;
		function onLoad() {
		// First loading opens the waiting page - we are still with the old sandbox.
	  	if (tabBrowser.currentURI.spec != 'about:blank') {
	     tabBrowser.loadURI('about:blank');
		    return;
	  	}
	   	// Here we are running with the right sandbox.
	  	tabBrowser.removeEventListener("load", onLoad, true);
	 	SJCore.configureWindow(tab, tabBrowser, aId, () => {
		    let url = aURL;
	    	if (!url) {
	      		url = Straitjacket.Common.sandboxen[aId].url;
	    	}
	     	if (typeof(url) != "string" || url.length == 0) {
	      		url = SJCore.README_URL;
	    	}
	    	try{
	    		tabBrowser.loadURI(url);
	    	} catch (e){
	    		Cu.reportError("[Straitjacket:] Error opening page "+url+"\n"+e);
	    	}
	    	SJCore.highlightBrowser(tab, tabBrowser);
	  	});
		}
	 	tabBrowser.addEventListener("load", onLoad, true);
	},
  	configureWindow : function(aTab, aBrowser, aId, aCb = null) {
		if (!aCb) { aCb = function(aStatus) {} }
	 	SJCore.sessionStore.setTabValue(aTab, SJCore.SJ_TAB_DATA_IDENTIFIER, JSON.stringify({ straitjacket: aId }));
	 	Straitjacket.Common.printlog("SJCore.configureWindow::getOA");
	 	try{
			SJCore.getOriginAttributes(aBrowser, (aAttr) => {
		  		if (SJCore.getSandboxFromOriginAttributes(aAttr) == aId) {
	    			aCb(false);
	    			return;
	  			}
				if ("firstPartyDomain" in aAttr) {
	     			aAttr.firstPartyDomain = aId ? "straitjacket-" + aId : "";
	  			} else {
		    		aAttr.appId = aId;
		  		}
	 			Straitjacket.Common.printlog("SJCore.configureWindow::setOA");
	  			SJCore.setOriginAttributes(aBrowser, aAttr, () => { aCb(true); });
			});
	 	} catch(e){
	 		Cu.reportError("[Straitjacket:] Error in SJCore.configureWindow: "+e);
	 	}
  	},
	configureWindowByName: function(aTab, aBrowser, aSandbox, aCb) {
		return SJCore.configureWindow(aTab, aBrowser, SJCore.getSandboxbyNameOrID(aSandbox,false), aCb);
  	},
  	highlightBrowser : function(aTab, aBrowser) {
		if (SJCore.defaultBrowserStyle === null) {
	  		SJCore.defaultBrowserStyle = aBrowser.style.border;
	  		SJCore.defaultTabStyle = aTab.style.color;
		}
	  	//Straitjacket.Common.printlog("SJCore.highlightBrowser::getOA");
		SJCore.getOriginAttributes(aBrowser, (aAttr) => {
	  		let id = SJCore.getSandboxFromOriginAttributes(aAttr);
	  		Straitjacket.Common.printlog("Highlight browser - id = "+id);
	  		if (!id) {
	    		aBrowser.style.border = SJCore.defaultBrowserStyle;
	    		aTab.style.color = SJCore.defaultTabStyle;
	    		return;
	  		}
			if (!(id in Straitjacket.Common.sandboxen)) {
	    		aBrowser.style.border = SJCore.defaultBrowserStyle;
	    		return;
	  		}
			Straitjacket.Common.reloadSandboxen();
			aBrowser.style.border = "3px solid " + Straitjacket.Common.sandboxen[id].color;
			aTab.style.color = Straitjacket.Common.sandboxen[id].color;
		});
	},
	getOriginAttributes : function(aBrowser, aCallback) {
		let docShell = aBrowser.contentWindow.QueryInterface(Ci.nsIInterfaceRequestor)
	    	                       .getInterface(Ci.nsIDocShell);
		aCallback(docShell.getOriginAttributes());
	},
	setOriginAttributes: function(aBrowser, aAttr, aCallback) {
		Straitjacket.Common.printlog("setOA-aAttr is "+JSON.stringify(aAttr));
/*		var mm = aBrowser.messageManager;
		if (mm) {
			Straitjacket.Common.printlog("setOriginAttributes::e10s mode");
			function scriptFunction() {
				function straitjacketmsg(aMsg) {
					removeMessageListener("straitjacket-oa", straitjacketmsg);
					console.log("calling set origin attributes");
					docShell.setOriginAttributes(aMsg.data);
					sendAsyncMessage("straitjacket-oa-done");
				};
				addMessageListener("straitjacket-oa", straitjacketmsg);
				sendAsyncMessage("straitjacket-oa-ready");
			}
			var scriptString = scriptFunction.toString();
			var scriptSource = scriptString.substring(scriptString.indexOf('\n') + 1, scriptString.length - 1);
			mm.loadFrameScript('data:,' + scriptSource, true);
			var listener = {
				receiveMessage: function(aMsg) {
					switch (aMsg.name) {
						case 'straitjacket-oa-ready':
							mm.removeMessageListener("straitjacket-oa-ready", listener);
							mm.sendAsyncMessage("straitjacket-oa", aAttr);
						break;
						case 'straitjacket-oa-done':
							mm.removeMessageListener("straitjacket-oa-done", listener);
							aCallback();
						break;
					}
				}
			}
			mm.addMessageListener("straitjacket-oa-ready", listener);
			mm.addMessageListener("straitjacket-oa-done", listener);
			return;
		}*/
		Straitjacket.Common.printlog("setOriginAttributes::non e10s mode");
		let docShell = aBrowser.contentWindow.QueryInterface(Ci.nsIInterfaceRequestor)
	                           .getInterface(Ci.nsIDocShell);
		docShell.setOriginAttributes(aAttr);
		aCallback();
	},
	getSandboxFromOriginAttributesInternal: function(aAttr, aWhat, aType) {
		if (aType == "string") {
			if (aAttr[aWhat].indexOf("straitjacket-") != 0) {
	 			return 0;
			}
			let ret = aAttr[aWhat].replace("straitjacket-","");
			return ret;
		}
		return aAttr[aWhat];
  	},
	getSandboxFromOriginAttributes: function(aAttr) {
	if ("firstPartyDomain" in aAttr) {
	  return SJCore.getSandboxFromOriginAttributesInternal(aAttr, "firstPartyDomain", "string");
	}
	return SJCore.getSandboxFromOriginAttributesInternal(aAttr, "appId", "int");
  },
  tabRestoring : function(aEvent) {
	let tab = aEvent.originalTarget;
	SJCore.restoreTab(tab);
  },
	restoreTab : function(aTab) {
		let window = aTab.ownerDocument.defaultView;
		let browser = window.gBrowser.getBrowserForTab(aTab);
		let data = SJCore.sessionStore.getTabValue(aTab, SJCore.SJ_TAB_DATA_IDENTIFIER);
		Straitjacket.Common.printlog("in restore tab for "+browser.currentURI.spec+"\n data = "+data);
		if(data.length < 1){
			return;
		}
		try {
			data = JSON.parse(data);
		} catch(e) {
	  		Cu.reportError("[Straitjacket:] Error restoring tab "+e);
	  		return;
		}
		function restoreTabReal() {
			let id = "appId" in data ? data.appId : data.straitjacket;
			Straitjacket.Common.printlog("restoring "+id+"\ndata.appId - "+data.appId+"\ndata.straitjacket "+data.straitjacket);
			SJCore.configureWindow(aTab, browser, id);
			SJCore.highlightBrowser(aTab, browser);
		}
		if (browser.currentURI.spec == 'about:blank') {
			restoreTabReal();
			return;
		}
		let url = browser.currentURI.spec;
		Straitjacket.Common.printlog("restore url is "+url);
		function onLoad() {
			if (browser.currentURI.spec != 'about:blank') {
				Straitjacket.Common.printlog("Non blank, returning. "+browser.currentURI.spec);
				return;
			}
			browser.removeEventListener("load", onLoad, true);
			restoreTabReal();
			browser.loadURI(url);
		}
		browser.addEventListener("load", onLoad, true);
		browser.loadURI('about:blank');
	},
	/**
	 * Common menu event handler.
	 * @param {} event
	 */
	menuCtxListener : function(event){
		let id = event.target.id.substring(event.target.id.lastIndexOf('-')+1,event.target.id.length-1);
		let link = event.target.getAttribute("link");
		SJCore.openSandbox(Services.wm.getMostRecentWindow("navigator:browser"),
		SJCore.getSandboxbyNameOrID(event.target.getAttribute("sandbox")),link);
	}
}