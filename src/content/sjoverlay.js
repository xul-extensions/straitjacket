if ("undefined" == typeof (Straitjacket)) {
	var Straitjacket = {};
}
Components.utils.import("chrome://straitjacket/content/sjcommon.jsm", Straitjacket);
Cu.import("chrome://straitjacket/content/sjcore.jsm", Straitjacket);
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Straitjacket.Init = {
	tbBtn: null,
	cmdTbBtn: null,
	ctxMenuArea: null,
	ctxMenuSJ: null,
	ctxMenuSJAssign: null,
	ctxMenuSJPopup: null,
	cmdCtxAssign : null,
	cmdCtxLink: null,
	clickedLink: null,
	mnuTools: null,
	prefs : null,
	init() {
		let firstRun = Straitjacket.Common.prefBranch.getBoolPref("firstrun", true);
		if (firstRun) {
			Straitjacket.Common.prefBranch.setBoolPref("firstrun", false);
			Straitjacket.Common.prefBranch.setBoolPref("logging", false);
			Straitjacket.Common.installButton(document, "nav-bar", "tbbtn-straitjacket");
			Straitjacket.Common.installButton(document, "addon-bar", "tbbtn-straitjacket");
		}
		Straitjacket.Common.logoutput = Straitjacket.Common.prefBranch.getBoolPref("logging", false);
		Straitjacket.Common.reloadSandboxen();
		Straitjacket.Common.strBundle = document.getElementById("straitjacket-strings");
		Straitjacket.Init.tbBtn = document.getElementById("tbbtn-straitjacket");
		Straitjacket.Init.cmdTbBtn = document.getElementById("CmdSJOptions");
		Straitjacket.Init.cmdCtxAssign = document.getElementById("CmdSJCtxAssign");
		Straitjacket.Init.cmdCtxLink = document.getElementById("CmdSJCtxLink");
		Straitjacket.Init.ctxMenuArea = document.getElementById("contentAreaContextMenu");
		Straitjacket.Init.ctxMenuSJAssign = document.getElementById("mnu-straitjacket-assign");
		Straitjacket.Init.ctxMenuSJPopup = document.getElementById("mnu-straitjacket-popup");
		Straitjacket.Init.ctxMenuSJ = document.getElementById("mnu-straitjacket-context");
		Straitjacket.Init.mnuTools = document.getElementById("mnu-straitjacket-tools");
		var container = gBrowser.tabContainer;
		container.addEventListener("TabOpen", Straitjacket.Init.tabOpenListener, false);
		container.addEventListener("TabClose", Straitjacket.Init.tabCloseListener, false);
		AddonManager.addAddonListener(Straitjacket.Common.uninstallHandler);
		Services.obs.addObserver(Straitjacket.Common.uninstallHandler, "quit-application", false);
		Straitjacket.Init.prefListener.register();
		gBrowser.addProgressListener(Straitjacket.Init.tabSwitchListener);
		Straitjacket.Init.cmdTbBtn.addEventListener('command', Straitjacket.Init.onTbBtn, false);
		Straitjacket.Init.cmdCtxAssign.addEventListener('command', Straitjacket.Init.onAssign, false);
		Straitjacket.Init.cmdCtxLink.addEventListener('command', Straitjacket.Init.onLink, false);
		Straitjacket.Init.ctxMenuArea.addEventListener('popupshowing', Straitjacket.Init.checkLink, false);
		if (Straitjacket.Init.reloadTabs()) {
			setTimeout(Straitjacket.Init.reloadTabs, 0);
		}
	},
	/**
	 * Create dynamic sandbox menu items wherever required.
	 */
	createSandboxMenuitems(aWindow, aPopup, aPrefix) {
		let popup = aWindow.document.getElementById(aPopup);
		if (popup) {
			let menus = document.querySelectorAll("[id ^='straitjacket-" + aPrefix + "--']");
			for (let i = 0; i < menus.length; i++) {
				menus[i].removeEventListener('command', Straitjacket.Common.menuCtxListener);
			}
			popup.parentNode.removeChild(popup);
		}
		popup = aWindow.document.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "menupopup");
		popup.setAttribute("id", aPopup);
		if (aPrefix == "mnutools") {
			let optionsmenu = aWindow.document.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "menuitem");
			optionsmenu.setAttribute("id", "mnu-straitjacket-tools-options");
			optionsmenu.setAttribute("label", Straitjacket.Common.strBundle.getString("toolsmenuoptions"));
			optionsmenu.setAttribute("accesskey", Straitjacket.Common.strBundle.getString("toolsmenuoptionsacc"));
			optionsmenu.setAttribute("command", "CmdSJOptions");
			popup.insertBefore(optionsmenu, null);
			let separator = aWindow.document.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "separator");
			popup.insertBefore(separator, null);
		}
		for (let i in Straitjacket.Common.sandboxen) {
			Straitjacket.Init.createMenuItem(aWindow, popup, Straitjacket.Common.sandboxen[i].name, aPrefix);
		}
		return popup;
	},
	/**
	 * Add menu items for sandboxes to link context menu.
	 * 
	 * @param {}
	 *            aWindow
	 */
	createLinkCtxMenu(aWindow, aMenu, aMenupopup) {
		let ctxMenuSJ = aWindow.document.getElementById(aMenu);
		let ctxMenuSJPopup = aWindow.document.getElementById(aMenupopup);
		Straitjacket.Common.reloadSandboxen();
		if (ctxMenuSJ.itemCount > 2) {
			let toRemove = [];
			for (let i = 2; i < ctxMenuSJ.itemCount; i++) {
				let item = ctxMenuSJ.getItemAtIndex(i);
				if (item.id.startsWith("straitjacket-linkctx--")) {
					toRemove.push(item);
				}
			}
			for (let j = 0; j < toRemove.length; j++) {
				ctxMenuSJ.removeItemAt(ctxMenuSJ.getIndexOfItem(toRemove[j])).removeEventListener('command', Straitjacket.SJCore.menuCtxListener);
			}
		}
		// Add sandbox menu items.
		for (let i = 0; i < Object.values(Straitjacket.Common.sandboxen).length; i++) {
			Straitjacket.Init.createMenuItem(aWindow, ctxMenuSJPopup, Object.values(Straitjacket.Common.sandboxen)[i].name, 'linkctx');
		}
	},
	/**
	 * Show context menu option when in sandbox tab.
	 */
	checkLink(event) {
		Straitjacket.Init.ctxMenuSJ = document.getElementById("mnu-straitjacket-context");
		gContextMenu.showItem('mnu-straitjacket-context', gContextMenu.onLink && Object.values(Straitjacket.Common.sandboxen).length > 0);
		let sandboxID = 0;
		//Straitjacket.Common.printlog("Init.checkLink::getOA");
		Straitjacket.SJCore.getOriginAttributes(gBrowser, function(aAttr) {
			sandboxID = Straitjacket.SJCore.getSandboxFromOriginAttributes(aAttr);
		if(sandboxID != 0){
			let label = Straitjacket.Common.strBundle.getFormattedString("assigntosandbox",[gBrowser.currentURI.host,
			Straitjacket.SJCore.getSandboxbyNameOrID(sandboxID,true)]);
			Straitjacket.Init.ctxMenuSJAssign.label = label;
			Straitjacket.Init.ctxMenuSJAssign.setAttribute("sandboxID",sandboxID);
			Straitjacket.Init.ctxContentPrefHandler.sandboxID = sandboxID;
			Straitjacket.Common.contentPrefs.getByDomainAndName(gBrowser.currentURI.host,
				"straitjacket-sandbox",null,Straitjacket.Init.ctxContentPrefHandler);
			gContextMenu.showItem('mnu-straitjacket-assign', sandboxID != 0);
		}
		});
		if (gContextMenu.onLink) {
			Straitjacket.Init.clickedLink = event.target.triggerNode.getAttribute("href");
			for (let i = 0; i < Straitjacket.Init.ctxMenuSJ.itemCount; i++) {
				if (Straitjacket.Init.ctxMenuSJ.getItemAtIndex(i).id.startsWith("straitjacket-linkctx--")) {
					Straitjacket.Init.ctxMenuSJ.getItemAtIndex(i).setAttribute("link", Straitjacket.Init.clickedLink);
				}
			}
			gContextMenu.showItem('mnu-straitjacket-context-link', sandboxID != 0);
		}
	},
	/**
	 * nsIContentPrefCallback2 implementation used for assigning sandboxes 
	 * to site content prefs from page context menu.
	 */
	ctxContentPrefHandler : {
		success : false,
		sandboxID : 0,
		handleCompletion(reason){
			if(reason == Ci.nsIContentPrefCallback2.COMPLETE_OK){
				if(!Straitjacket.Init.ctxContentPrefHandler.success){
					Straitjacket.Common.printlog("No sandbox assigned.");
					Straitjacket.Init.ctxMenuSJAssign.removeAttribute("checked");
				}
			} else {
				Straitjacket.Common.printlog("Context menu content pref operation not completed");
			}
		},
		handleError(error){
			Cu.reportError("[Straitjacket:] Error handling content preferences for context menu. "+error);
		},
		handleResult(pref){
			Straitjacket.Init.ctxContentPrefHandler.success = true;
			Straitjacket.Common.printlog("Current sandbox  is "+Straitjacket.SJCore.getSandboxbyNameOrID(Straitjacket.Init.ctxContentPrefHandler.sandboxID,true)
				+"\nRetrieved assigned sandbox is "+Straitjacket.SJCore.getSandboxbyNameOrID(pref.value,true)+" for "+pref.domain);
			if(pref.value == Straitjacket.Init.ctxContentPrefHandler.sandboxID){
				Straitjacket.Init.ctxMenuSJAssign.setAttribute("checked","true");
			} else {
				Straitjacket.Init.ctxMenuSJAssign.removeAttribute("checked");
			}
		}
	},
	/**
	 * Event handler for link context menu in sandbox tab.
	 */
	onLink() {
		//Straitjacket.Common.printlog("Init.onLink::getOA");
		Straitjacket.SJCore.getOriginAttributes(gBrowser, function(aAttr) {
			let win = Services.wm.getMostRecentWindow("navigator:browser");
			let id = Straitjacket.SJCore.getSandboxFromOriginAttributes(aAttr);
			Straitjacket.SJCore.openSandbox(win, id, Straitjacket.Init.clickedLink);
		});
	},
	/**
	 * Event handler for context menu 'assign to sandbox' in sandbox tab.
	 */
	onAssign(){
		if(Straitjacket.Init.ctxMenuSJAssign.hasAttribute("checked")){
			Straitjacket.Common.contentPrefs.set(gBrowser.currentURI.host,"straitjacket-sandbox",
			Straitjacket.Init.ctxMenuSJAssign.getAttribute("sandboxID"),null);
		} else {
			Straitjacket.Common.contentPrefs.removeBySubdomainAndName(gBrowser.currentURI.host,"straitjacket-sandbox",null);
		}
	},
	reloadTabs() {
		if (!gBrowser.tabs.length) {
			Straitjacket.Common.printlog("reload tabs, zero length.");
			return false;
		}
		Straitjacket.Common.printlog("reloading " + gBrowser.tabs.length + " tabs.");
		for (let i = 0; i < gBrowser.tabs.length; i++) {
			Straitjacket.SJCore.restoreTab(gBrowser.tabs[i]);
		}
		return true;
	},
	refreshWindows() {
		var windows = Services.wm.getEnumerator('navigator:browser');
		var window;
		while (windows.hasMoreElements()) {
			window = windows.getNext().QueryInterface(Ci.nsIDOMWindow);
			Straitjacket.Init.configureWindow(window);
			for (let tab of window.gBrowser.tabs) {
				Straitjacket.Init.highlightBrowser(tab, window);
			}
		}
	},
	highlightBrowser(aTab, aWindow) {
		if (aWindow.document.documentElement.getAttribute('windowtype') != "navigator:browser") {
			return;
		}
		if (!aTab) {
			aTab = aWindow.gBrowser.selectedTab;
		}
		let browser = aWindow.gBrowser.getBrowserForTab(aTab);
		Straitjacket.SJCore.highlightBrowser(aTab, browser);
	},
	configureWindow(aWindow) {
		if (aWindow.document.documentElement.getAttribute('windowtype') != "navigator:browser") {
			return;
		}
		let oldmenu = aWindow.document.getElementById('mnu_straitjacket');
		if (oldmenu) {
			oldmenu.parentNode.removeChild(oldmenu);
		}
		let oldseparator = aWindow.document.getElementById('straitjacket_separator');
		if (oldseparator) {
			oldseparator.parentNode.removeChild(oldseparator);
		}
		let mainmenu = aWindow.document.getElementById("tabContextMenu");
		let separator = aWindow.document.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "menuseparator");
		separator.setAttribute('id', 'straitjacket_separator');
		mainmenu.insertBefore(separator, mainmenu.firstChild);
		let menu = aWindow.document.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "menu");
		menu.setAttribute('id', 'mnu_straitjacket');
		menu.setAttribute('label', Straitjacket.Common.strBundle.getString("tabmenucaption"));
		menu.setAttribute('class', 'menu-iconic');
		menu.setAttribute('image', 'chrome://straitjacket/skin/straitjacket-16.png');
		mainmenu.insertBefore(menu, mainmenu.firstChild);

		let popup = aWindow.document.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "menupopup");
		menu.appendChild(popup);
		popup.addEventListener('popupshown', function() {
			let tab = popup.triggerNode.localName == 'tab' ? popup.triggerNode : aWindow.gBrowser.selectedTab;
			let browser = aWindow.gBrowser.getBrowserForTab(tab);
			//Straitjacket.Common.printlog("Init.configureWindow::getOA");
			Straitjacket.SJCore.getOriginAttributes(browser, function(aAttr) {
				let item;
				let id = Straitjacket.SJCore.getSandboxFromOriginAttributes(aAttr);
				if (id) {
					item = aWindow.document.getElementById("straitjacket-tabctx--" + id);
				}
				if (!item) {
					item = aWindow.document.getElementById("straitjacket-tabctx");
				}
				item.setAttribute("checked", "true");
			});
		});
		Straitjacket.Common.reloadSandboxen();
		Straitjacket.Init.createLinkCtxMenu(aWindow, "mnu-straitjacket-context", "mnu-straitjacket-popup");
		Straitjacket.Init.mnuTools.appendChild(Straitjacket.Init.createSandboxMenuitems(aWindow, "mnu-straitjacket-tools-popup", "mnutools"));
		Straitjacket.Init.dynamicMenuHandler.aPopup = popup;
		Straitjacket.Init.dynamicMenuHandler.aWindow = aWindow;
		Straitjacket.Init.createMenuItem(aWindow, popup, null, 'tabctx', Straitjacket.Init.dynamicMenuHandler.tabCtxListener);
		separator = aWindow.document.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "menuseparator");
		popup.appendChild(separator);
		for (let i in Straitjacket.Common.sandboxen) {
			Straitjacket.Init.dynamicMenuHandler.aSandbox = Straitjacket.Common.sandboxen[i].name;
			Straitjacket.Init.createMenuItem(aWindow, popup, Straitjacket.Common.sandboxen[i].name, 'tabctx', Straitjacket.Init.dynamicMenuHandler.tabCtxListener);
		}
		if (Straitjacket.Init.tbBtn) {
			Straitjacket.Init.tbBtn.appendChild(Straitjacket.Init.createSandboxMenuitems(aWindow, "straitjacket-tb-popup", "tbbtn"));
		}
		aWindow.document.removeEventListener("SSTabRestoring", Straitjacket.Init.tabRestoring);
		aWindow.document.addEventListener("SSTabRestoring", Straitjacket.Init.tabRestoring, false);
	},
	tabRestoring(aEvent) {
		Straitjacket.SJCore.tabRestoring(aEvent);
	},
	createMenuItem(aWindow, aPopup, aSandbox, aPrefix, callback) {
		let item = aWindow.document.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "menuitem");
		aPopup.appendChild(item);
		item.setAttribute("label", aSandbox ? aSandbox : Straitjacket.Common.strBundle.getString("defaultsandboxlabel"));
		item.setAttribute('name', 'straitjacket');
		item.setAttribute('type', 'radio');
		if (aSandbox) {
			item.setAttribute('id', 'straitjacket-' + aPrefix + '--' + Straitjacket.SJCore.getSandboxbyNameOrID(aSandbox,false));
			item.setAttribute('sandbox', aSandbox);
			item.setAttribute("tooltiptext", Straitjacket.Common.strBundle.getFormattedString("sandboxtooltip", [aSandbox]));
		} else {
			item.setAttribute('id', 'straitjacket-' + aPrefix);
		}
		item.addEventListener('command', aPrefix == 'tabctx' ? callback : Straitjacket.SJCore.menuCtxListener, false);
	},
	dynamicMenuHandler: {
		aPopup: null,
		aWindow: null,
		aSandbox: null,
		tabCtxListener(event) {
			let tab = Straitjacket.Init.dynamicMenuHandler.aPopup.triggerNode.localName == 'tab' ?
				Straitjacket.Init.dynamicMenuHandler.aPopup.triggerNode :
				Straitjacket.Init.dynamicMenuHandler.aWindow.gBrowser.selectedTab;
			let browser = Straitjacket.Init.dynamicMenuHandler.aWindow.gBrowser.getBrowserForTab(tab);
			if(!browser.currentURI.scheme.startsWith('http')){
				return;
			}
			Straitjacket.Init.dynamicMenuHandler.aSandbox = event.target.getAttribute("sandbox");
			//Straitjacket.Common.printlog("Init.tabCtxListener::getOA");
			Straitjacket.SJCore.getOriginAttributes(browser, (aAttr) => {
				if (aAttr != Straitjacket.Init.dynamicMenuHandler.aSandbox) {
					let url = browser.currentURI.spec;
					function onLoad() {
						if (browser.currentURI.spec != 'about:blank') {
							return;
						}
						browser.removeEventListener("load", onLoad, true);
						Straitjacket.SJCore.configureWindowByName(tab, browser, Straitjacket.Init.dynamicMenuHandler.aSandbox, function(aStatus) {
							if (aStatus) {
								Straitjacket.Init.dynamicMenuHandler.aWindow.gBrowser.reloadTab(tab);
								Straitjacket.SJCore.highlightBrowser(tab, Straitjacket.Init.dynamicMenuHandler.aWindow.gBrowser.getBrowserForTab(tab));
								Straitjacket.Common.printlog("Loading "+url.spec);
								browser.loadURI(url);
							}
						});
					}
					browser.addEventListener("load", onLoad, true);
					browser.loadURI('about:blank');
				}
			});
		}
	},
	tabCloseListener() {

	},
	tabOpenListener(event) {
		let browserWindow = gBrowser.getBrowserForTab(event.target).contentWindow.QueryInterface(Ci.nsIInterfaceRequestor)
			.getInterface(Ci.nsIWebNavigation).QueryInterface(Ci.nsIDocShellTreeItem)
			.rootTreeItem.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);
		Straitjacket.Init.configureWindow(browserWindow);
	},
	tabSwitchListener: {
		success : false,
		currURI : null,
		QueryInterface: XPCOMUtils.generateQI(["nsIWebProgressListener", "nsISupportsWeakReference"]),
		onStateChange(aWebProgress, aRequest, aFlag, aStatus) {
		},
		onLocationChange(aProgress, aRequest, aURI) {
			if (!aURI.scheme.startsWith('http')) {
				return;
			}
			Straitjacket.Init.tabSwitchListener.currURI = aURI;
			let browserWindow = gBrowser.getBrowserForTab(gBrowser.selectedTab).contentWindow
				.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIWebNavigation)
				.QueryInterface(Ci.nsIDocShellTreeItem).rootTreeItem.QueryInterface(Ci.nsIInterfaceRequestor)
				.getInterface(Ci.nsIDOMWindow);
			Straitjacket.Init.highlightBrowser(null, browserWindow);
			Straitjacket.Common.contentPrefs.getByDomainAndName(aURI.host,"straitjacket-sandbox",
				null,Straitjacket.Init.tabSwitchListener);
			Straitjacket.Common.printlog("location change " + aURI.spec + "\nbrowser is " + gBrowser.selectedBrowser.currentURI.spec);
		},
		loadSandbox(sandboxID){
				let tab = gBrowser.selectedTab;
				let browser = gBrowser.selectedBrowser;
				Straitjacket.SJCore.configureWindowByName(tab, browser, sandboxID, function(aStatus) {
					if (aStatus) {
						gBrowser.reloadTab(tab);
						Straitjacket.SJCore.highlightBrowser(tab, gBrowser.getBrowserForTab(tab));
						browser.loadURI(Straitjacket.Init.tabSwitchListener.currURI);
					}
				});
		},
		handleCompletion (reason){
			if(reason == Straitjacket.Ci.nsIContentPrefCallback2.COMPLETE_ERROR || !Straitjacket.Init.tabSwitchListener.success){
				let sandboxID = 0;
				Straitjacket.SJCore.getOriginAttributes(gBrowser, function(aAttr) {
					sandboxID = Straitjacket.SJCore.getSandboxFromOriginAttributes(aAttr);
				});
				if (sandboxID != 0 && Straitjacket.Init.tabSwitchListener.currURI) {
					Straitjacket.Common.printlog("Captured request to " + Straitjacket.Init.tabSwitchListener.currURI.spec);
					Straitjacket.Init.tabSwitchListener.loadSandbox(sandboxID);
				}
			}
		},
		handleError(error){
			Cu.reportError("[Straitjacket:] Error handling content preferences for tab switch. "+error);
		},
		handleResult(pref){
			Straitjacket.Common.printlog("Got pref "+pref.value+" for "+pref.domain+"\ncurr URI is "+Straitjacket.Init.tabSwitchListener.currURI.host);
			Straitjacket.Init.tabSwitchListener.success = true;
			if(Straitjacket.Init.tabSwitchListener.currURI.host == pref.domain){
				Straitjacket.Init.tabSwitchListener.loadSandbox(pref.value);
			}
		}
	},
	prefListener: new Straitjacket.SJPrefListener("extensions.straitjacket.", function(branch, name) {
		switch (name) {
			case "logging": Straitjacket.Common.logoutput = branch.getBoolPref("logging", false);
				break;
			case "sandboxen": Straitjacket.Init.refreshWindows();
				break;
			case "toolsmenu":
				branch.getBoolPref("toolsmenu", false) ? Straitjacket.Init.mnuTools.removeAttribute("hidden") : Straitjacket.Init.mnuTools.setAttribute("hidden", "true");
				break;
		}
	}),
	onTbBtn() {
		if (null == Straitjacket.Common.optionsDlg || Straitjacket.Common.optionsDlg.closed) {
			let instantApply =
				Services.prefs.getBoolPref("browser.preferences.instantApply", true);
			let features =
				"chrome,titlebar,centerscreen" +
				(instantApply.value ? ",dialog=no" : ",modal");
			Straitjacket.Common.optionsDlg = window.openDialog("chrome://straitjacket/content/sjsettings.xul",
				"Straitjacket Options", features);
			Straitjacket.Common.optionsDlg.focus();
		}
	},
	cleanup() {
		Straitjacket.Init.ctxMenuArea.removeEventListener('popupshowing', Straitjacket.Init.checkLink);
		Straitjacket.Init.cmdCtxLink.removeEventListener('command', Straitjacket.Init.onLink);
		Straitjacket.Init.cmdCtxAssign.removeEventListener('command', Straitjacket.Init.onAssign);
		Straitjacket.Init.cmdTbBtn.removeEventListener('command', Straitjacket.Init.onTbBtn);
		gBrowser.removeProgressListener(Straitjacket.Init.tabSwitchListener);
		Straitjacket.Init.prefListener.unregister();
		Services.obs.removeObserver(Straitjacket.Common.uninstallHandler, "quit-application");
		AddonManager.removeAddonListener(Straitjacket.Common.uninstallHandler);
		var container = gBrowser.tabContainer;
		container.removeEventListener("TabClose", Straitjacket.Init.tabCloseListener);
		container.removeEventListener("TabOpen", Straitjacket.Init.tabOpenListener);
	}
}
window.addEventListener("load", Straitjacket.Init.init, false);
window.addEventListener("unload", Straitjacket.Init.cleanup, false);