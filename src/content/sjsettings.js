if("undefined" == typeof (Straitjacket)) {
	var Straitjacket = {};
}
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("chrome://straitjacket/content/sjcommon.jsm",Straitjacket);
Cu.import("chrome://straitjacket/content/sjcore.jsm",Straitjacket);
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Straitjacket.Sandboxen = {
	bdEdit : null,
	bdList : null,
	chkTools : null,
	chkLog : null,
	cmdNew : null,
	cmdEdit : null,
	cmdDelete : null,
	editDialog : null,
	edit : false,
	lstSandboxen : null,
	returnparams : null,
	todelete : [],
	init : function(){
		Straitjacket.Sandboxen.bdEdit = document.getElementById("bd-sjeditboxenopen");
		Straitjacket.Sandboxen.bdList = document.getElementById("bd-sjlistselected");
		Straitjacket.Sandboxen.chkLog = document.getElementById("straitjacket-options-chklog");
		Straitjacket.Sandboxen.chkTools = document.getElementById("straitjacket-options-chktoolsmenu");
		Straitjacket.Sandboxen.cmdNew = document.getElementById("cmdSJNew");
		Straitjacket.Sandboxen.cmdEdit = document.getElementById("cmdSJEdit");
		Straitjacket.Sandboxen.cmdDelete = document.getElementById("cmdSJDelete");
		Straitjacket.Sandboxen.lstSandboxen = document.getElementById("lst-straitjacket-sandboxes");
		Straitjacket.Sandboxen.chkLog.checked = Straitjacket.Common.prefBranch.getBoolPref("logging",false);
		Straitjacket.Sandboxen.chkTools.checked = Straitjacket.Common.prefBranch.getBoolPref("toolsmenu",false);
		Straitjacket.Sandboxen.chkLog.addEventListener("command",Straitjacket.Sandboxen.checkboxListener,false);
		Straitjacket.Sandboxen.chkTools.addEventListener("command",Straitjacket.Sandboxen.checkboxListener,false);
		Straitjacket.Sandboxen.cmdNew.addEventListener("command",Straitjacket.Sandboxen.onNew,false);
		Straitjacket.Sandboxen.cmdEdit.addEventListener("command",Straitjacket.Sandboxen.onEdit,false);
		Straitjacket.Sandboxen.cmdDelete.addEventListener("command",Straitjacket.Sandboxen.onDelete,false);
		Straitjacket.Sandboxen.lstSandboxen.addEventListener("select",Straitjacket.Sandboxen.onSelect,false);
		Straitjacket.Sandboxen.loadSandboxen();
		Straitjacket.Sandboxen.lstSandboxen.selectedIndex = -1;
	},
	checkboxListener : function(event){
		switch(event.target){
			case Straitjacket.Sandboxen.chkLog : Straitjacket.Common.prefBranch.setBoolPref("logging",event.target.checked);
				break;
			case Straitjacket.Sandboxen.chkTools :	Straitjacket.Common.prefBranch.setBoolPref("toolsmenu",event.target.checked);
		}
	},
	loadSandboxen : function(){
		if(Straitjacket.Common.sandboxen != {}){
			for(let i in Straitjacket.Common.sandboxen){
				let listcell = Straitjacket.Sandboxen.lstSandboxen.appendItem(Straitjacket.Common.sandboxen[i].name,Straitjacket.Common.sandboxen[i].id);
				listcell.setAttribute("style","background-color:"+Straitjacket.Common.sandboxen[i].color);
			}
		}
	},
	/**
	 * Create new sandbox.
	 */
	onNew : function(){
		Straitjacket.Sandboxen.launchEdit(null);
	},
	/**
	 * Edit existing sandbox from list.
	 */
	onEdit : function(){
		edit = true;
		Straitjacket.Sandboxen.cmdEdit.setAttribute('disabled','true');
		Straitjacket.Sandboxen.lstSandboxen.setAttribute('disabled','true');
		let selsandbox = Object.values(Straitjacket.Common.sandboxen)[Straitjacket.Sandboxen.lstSandboxen.selectedIndex];
		if(selsandbox != "undefined"){
			Straitjacket.Sandboxen.launchEdit(selsandbox);
		} else{
			Straitjacket.Sandboxen.launchEdit(Straitjacket.Sandboxen.returnparams);
		}
	},
	/**
	 * Delete selected sandbox.
	 */
	onDelete : function(event){
		let selsandbox = Straitjacket.Common.sandboxen[Straitjacket.Sandboxen.lstSandboxen.selectedItem.value];
		Straitjacket.Common.printlog("to delete "+Straitjacket.Sandboxen.lstSandboxen.selectedItem.value + " "+JSON.stringify(selsandbox));
		if(selsandbox != "undefined" && selsandbox != null){
			Straitjacket.Sandboxen.todelete.push(selsandbox);
		}
		Straitjacket.Sandboxen.lstSandboxen.removeItemAt(Straitjacket.Sandboxen.lstSandboxen.selectedIndex);
	},
	/**
	 * List selection listener.
	 */	
	onSelect : function(event){
		let disabledFlag = Straitjacket.Sandboxen.lstSandboxen.selectedIndex == -1 ? 'true' : 'false';
		Straitjacket.Sandboxen.bdList.setAttribute('disabled', disabledFlag);
	},
	reenableControls(){
		Straitjacket.Sandboxen.cmdEdit.setAttribute('disabled','false');
		Straitjacket.Sandboxen.lstSandboxen.setAttribute('disabled','false');
	},
	/**
	 * Callback after clicking Ok on edit window.
	 */
	onEditDone(param){
		Straitjacket.Sandboxen.returnparams = param;
		if(Straitjacket.Sandboxen.returnparams.hasOwnProperty("id")){
			Straitjacket.Sandboxen.lstSandboxen.selectedItem.label = Straitjacket.Sandboxen.returnparams.name;
			Straitjacket.Sandboxen.lstSandboxen.selectedItem.setAttribute("style","background-color:"+Straitjacket.Sandboxen.returnparams.color);
		} else {
			let id = Straitjacket.SJCore.generateID();
			Straitjacket.Sandboxen.returnparams.id = id;
			let listcell = Straitjacket.Sandboxen.lstSandboxen.appendItem(Straitjacket.Sandboxen.returnparams.name,Straitjacket.Sandboxen.returnparams.id);
			listcell.setAttribute("style","background-color:"+Straitjacket.Sandboxen.returnparams.color);
		}
		Straitjacket.Sandboxen.reenableControls();
	},
	onOk : function(){
		if(Straitjacket.Sandboxen.returnparams != null){
			Straitjacket.Common.sandboxen[Straitjacket.Sandboxen.returnparams.id] = Straitjacket.Sandboxen.returnparams;
		}
		if(Straitjacket.Sandboxen.todelete.length > 0){
			for(item in Straitjacket.Sandboxen.todelete){
				Straitjacket.Sandboxen.deleteSandbox(Straitjacket.Sandboxen.todelete[item].id);
				delete Straitjacket.Common.sandboxen[Straitjacket.Sandboxen.todelete[item].id];
			}
		}
		Straitjacket.Common.prefBranch.setCharPref("sandboxen",JSON.stringify(Straitjacket.Common.sandboxen));
//		Services.obs.notifyObservers(null,"straitjacket-refresh",null);
	},
	/**
	 * Delete and clean up sandbox.
	 */
	deleteSandbox : function(aId){
		let subject = {
			appId: aId,
			browserOnly: false,
			QueryInterface: XPCOMUtils.generateQI([Ci.mozIApplicationClearPrivateDataParams])
		};
		const serviceMarker = "service,";
		let cm = Cc["@mozilla.org/categorymanager;1"].getService(Ci.nsICategoryManager);
		let enumerator = cm.enumerateCategory("webapps-clear-data");
		let observers = [];
		while (enumerator.hasMoreElements()) {
			let entry = enumerator.getNext().QueryInterface(Ci.nsISupportsCString).data;
			let contractID = cm.getCategoryEntry("webapps-clear-data", entry);
			let factoryFunction;
			if (contractID.substring(0, serviceMarker.length) == serviceMarker) {
				contractID = contractID.substring(serviceMarker.length);
				factoryFunction = "getService";
			} else {
				factoryFunction = "createInstance";
			}
			try {
				let handler = Cc[contractID][factoryFunction]();
				if (handler) {
					let observer = handler.QueryInterface(Ci.nsIObserver);
					observers.push(observer);
				}
			} catch(e) { 
			}
		}
		enumerator = Services.obs.enumerateObservers("webapps-clear-data");
		while (enumerator.hasMoreElements()) {
			try {
				let observer = enumerator.getNext().QueryInterface(Ci.nsIObserver);
				if (observers.indexOf(observer) == -1) {
					observers.push(observer);
				}
			} catch (e) { 
			}
		}
	observers.forEach(function (observer) {
			try {
				observer.observe(subject, "webapps-clear-data", null);
			} catch(e) { 
			}
		});
	},
	/**
	 * Opens edit window.
	 */
	launchEdit : function(params){
		if(Straitjacket.Sandboxen.editDialog == null || Straitjacket.Sandboxen.editDialog.closed){
				let instantApply = Services.prefs.getBoolPref("browser.preferences.instantApply",true);
				let features = "chrome,titlebar,centerscreen" +
					(instantApply.value ? ",dialog=no" : ",modal");
			window.openDialog("chrome://straitjacket/content/sjsandboxedit.xul","straitjacket-sandboxedit",
			features,params,Straitjacket.Sandboxen.onEditDone,Straitjacket.Sandboxen.reenableControls);
		}
	},
	cleanup : function() {
		Straitjacket.Sandboxen.lstSandboxen.removeEventListener("selected",Straitjacket.Sandboxen.onSelect);
		Straitjacket.Sandboxen.cmdDelete.removeEventListener("command",Straitjacket.Sandboxen.onDelete);
		Straitjacket.Sandboxen.cmdEdit.removeEventListener("command",Straitjacket.Sandboxen.onEdit);
		Straitjacket.Sandboxen.cmdNew.removeEventListener("command",Straitjacket.Sandboxen.onNew);
		Straitjacket.Sandboxen.chkTools.removeEventListener("command",Straitjacket.Sandboxen.checkboxListener);
		Straitjacket.Sandboxen.chkLog.removeEventListener("command",Straitjacket.Sandboxen.checkboxListener);
	}
}
window.addEventListener("load", Straitjacket.Sandboxen.init, false);
window.addEventListener("unload",Straitjacket.Sandboxen.cleanup,false);